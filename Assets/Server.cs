﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Text;
using Jayrock.JsonRpc;

public class Server : MonoBehaviour {
	public string interfacename = "0.0.0.0";
	public int port = 3333;

	// Use this for initialization
	void Start () {
		StartCoroutine("Listen");
	}

	IEnumerator Listen() {
		var address = IPAddress.Parse(interfacename);
		while (enabled) {
			using (var listener = new DisposableListener(address, port)) {
				listener.Start();
				while (listener.Active) {
					yield return null;
					if (!listener.Pending())
						continue;
					StartCoroutine(Catch(Process(listener)));
				}
			}
		}
	}

	IEnumerator Catch(IEnumerator coroutine) {
		while (true) {
			try {
				if (!coroutine.MoveNext())
					break;
			} catch (System.Exception e) {
				Debug.Log(e);
			}
			yield return coroutine.Current;
		}
	}

	IEnumerator Process(TcpListener listener) {
		using (var client = listener.AcceptTcpClient())
		using (var stream = client.GetStream()) {
			Debug.Log("Connected");
			var encoding = new UTF8Encoding(false);
			var decoder = encoding.GetDecoder();
			var request = new StringBuilder();
			var readBuf = new byte[client.ReceiveBufferSize];
			var readChars = new char[readBuf.Length];
			while (true) {
				yield return null;
				if (!stream.DataAvailable)
					continue;
				var byteCount = stream.Read(readBuf, 0, readBuf.Length);
				var charCount = decoder.GetCharCount(readBuf, 0, byteCount);
				if (charCount == 0)
					continue;
				decoder.GetChars(readBuf, 0, byteCount, readChars, 0);
				request.Append(readChars, 0, charCount);
				var lastChar = readChars[charCount - 1];
				if (lastChar == '\u000a' || lastChar == '\u000d')
					break;
				Debug.Log("Waiting");
			}
			var dispatcher = new JsonRpcDispatcher(new Service());
			var response = dispatcher.Process(request.ToString());
			var writeBuf = encoding.GetBytes(response);
			stream.Write(writeBuf, 0, writeBuf.Length);
			stream.Flush();
		}
		Debug.Log("Disconnected");
	}
	
	public class DisposableListener : TcpListener, System.IDisposable {
		public DisposableListener(IPEndPoint endpoint) : base(endpoint) {}
		public DisposableListener(IPAddress address, int port) : base(address, port) {}

		public bool Active { get { return base.Active; } }

		#region IDisposable implementation
		public void Dispose () {
			try {
				Stop();
			} catch {}
		}
		#endregion
	}

	class Service : JsonRpcService {
		[ JsonRpcMethod("add") ]
		public int Add(int a, int b) { return a + b; }
		[ JsonRpcMethod("env") ]
		public IDictionary GetEnvironment() { return System.Environment.GetEnvironmentVariables(); }
	}
}
