﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Text;
using Jayrock.JsonRpc;
using System.Threading;

public class Client : MonoBehaviour {
	public string interfacename = "0.0.0.0";
	public int port = 3333;
	public int maxClients = 1;

	private int _nClients;
	private object _locker = new object();

	void Update() {

		lock(_locker) {
			while (_nClients < maxClients) {
				_nClients++;
				ThreadPool.QueueUserWorkItem(Process);
			}
		}
	}

	void Process(object status) {
		try {
			var address = IPAddress.Parse(interfacename);
			using (var client = new TcpClient()) {
				client.Connect(interfacename, port);
				while (!client.Connected) {}
				using (var stream = client.GetStream())
				using (var reader = new StreamReader(stream, Encoding.UTF8))
				using (var writer = new StreamWriter(stream, new UTF8Encoding(false))) {
					writer.WriteLine("{id:1,method:add,params:[123,456]}");
					writer.Flush();
					Debug.Log("Response : " + reader.ReadLine());
				}
			}
		} finally {
			lock(_locker) {
				_nClients--;
			}
		}
	}
}
