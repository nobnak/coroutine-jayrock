﻿using System.Collections.Generic;

public class ConcurrentQueue<T> {
	private Queue<T> _queue = new Queue<T>();
	private object _locker = new object();

	public void Enqueue(T additional) {
		lock(_locker) {
			_queue.Enqueue(additional);
		}
	}

	public bool TryDequeue(out T removal) {
		lock(_locker) {
			removal = default(T);
			if (_queue.Count == 0)
				return false;
			removal = _queue.Dequeue();
			return true;
		}
	}
}
